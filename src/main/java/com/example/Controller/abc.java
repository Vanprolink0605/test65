package com.example.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller// Chỉ định HomeController là Controller
public class abc {
    // Khi user truy cập vào endpoint / thì homepage() được gọi
    @GetMapping("/abc")
    public String homepage() {
        return "abc";  // Trả về trang index.html
    }
    
    // Có thể mapping thêm các endpoint khác nữa...
}