package com.example.DenoNGS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DenoNgsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DenoNgsApplication.class, args);
	}

}
